using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using EasyModbus;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using smartlocker.hardware.apiv2.Models;
using smartlocker.hardware.apiv2.Services.Interfaces;
using SmartLockerProject.Web.API.Models;
using SmartLockerProject.Web.API.Models.Output;
//using smartlocker.hardware.apiv2.Models;

namespace smartlocker.hardware.apiv2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LockersController : ControllerBase
    {
        private readonly ILockerService lockerService;
        private readonly IRegisterService registerService;

        private readonly ResponseHeader respH = new ResponseHeader();

        private readonly ModbusClient modbusClient;
        public LockersController(ILockerService lockerService , IRegisterService registerService)
        {
            this.lockerService = lockerService;
            this.registerService = registerService;
            //modbusClient = new ModbusClient("COM4")
            //{
            //    Parity = System.IO.Ports.Parity.None,
            //    StopBits = StopBits.One
            //};

        }
        //[HttpGet("")]
        //public IActionResult GetLockerAll([FromQuery] int lockerId)
        //{
        //    try
        //    {
        //        var data = lockerService.GetAllLocker(lockerId);
                
        //        return StatusCode(StatusCodes.Status200OK, respH.ResponseH("S", "Locker Sucessful", data));
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status400BadRequest, respH.ResponseH("F", "Locker UnSucessful", e.Message));
        //    }//finally
        //    finally{
        //        modbusClient.Disconnect();
        //    }
        //}

        //refactored
        [HttpGet("checkStatus")]
        public IActionResult CheckStatus([FromQuery] int lkRoomId)
        {
            try
            {
                
                var data = lockerService.checkBooking(lkRoomId);
                if (data == true)
                    return StatusCode(StatusCodes.Status200OK, respH.ResponseH("S", "Locker found", data));
                else
                    throw new ArgumentException("invalid booking in this locker room id");
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, respH.ResponseH("F", e.Message, null));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, respH.ResponseH("F", e.Message, e.StackTrace));
            }
        }

        [HttpGet("lkroom")]
        public IActionResult GetLkRoomByLockerId([FromQuery] int lockerId)
        {
            var statusCodes = StatusCodes.Status200OK;
            //modbusClient.Connect();

            try
            {
                var data = lockerService.GetAllLocker(lockerId);
                //set up register before start
                foreach (Locker locker in data)
                {
                    int realStartAddr = registerService.GetRealStartAttr(locker.RegisterId.Value);
                    int[] command = new int[4];
                    if (locker.Status.Equals("A"))
                    {
                        command = registerService.SetRegisterToWrite(1, 0, 0, 0);
                    }
                    else if (locker.Status.Equals("B") || locker.Status.Equals("F"))
                    {
                        command = registerService.SetRegisterToWrite(0, 1, 0, 0);
                    }
                    //modbusClient.WriteMultipleRegisters(realStartAddr, command);
                }
                //modbusClient.WriteSingleRegister(41, 1);
                //end of set up register before start

                return StatusCode(statusCodes, respH.ResponseH("S", "Locker Sucessful", data));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, respH.ResponseH("F", e.Message, e.StackTrace));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, respH.ResponseH("F", e.Message, e.StackTrace));
            }
            finally
            {
                //modbusClient.Disconnect();
            }
        }


        [HttpGet("openlkroom")]
        public IActionResult OpenLkRoom(
           [FromQuery] string password,
           [FromQuery] int lkRoomId,
           [FromQuery] int? id
           )
        {
            try
            {
                //modbusClient.Connect();
                int realStartAddr = registerService.GetRealStartAttr(id.Value);
                if (String.IsNullOrEmpty(password))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, respH.ResponseH("F", "Password required", null));
                }
                //Service check password
                var bookingResult = lockerService.Authentication(password, lkRoomId);

                //start connect with locker here
                if (bookingResult.Status == "WP")
                {
                    //modbusClient.WriteMultipleRegisters(realStartAddr, registerService.OpenLockerRoomProcess());
                }
                else if (bookingResult.Status == "WF")
                {
                    //modbusClient.WriteMultipleRegisters(realStartAddr, registerService.OpenLockerRoomFinish());
                }
                else
                {
                    throw new Exception("Cannot update booking bacause status invalid");
                }
                // var result = modbusClient.ReadHoldingRegisters(realStartAddr, 4);
                //end of connect with locker here

                //start update booking
                lockerService.UpdateBooking(bookingResult);
                //end update booking

                return StatusCode(StatusCodes.Status200OK, respH.ResponseH("S", "Open Locker Successful", bookingResult));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, respH.ResponseH("F", e.Message, e.StackTrace));
            }
            catch (UnauthorizedAccessException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, respH.ResponseH("F", e.Message, e.StackTrace));
            }
            catch (TimeoutException e)
            {
                var response = respH.ResponseH("F", e.Message, null);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, respH.ResponseH("F", e.Message, e.StackTrace));
            }
            finally
            {
                //modbusClient.Disconnect();
            }

        }


    }
}