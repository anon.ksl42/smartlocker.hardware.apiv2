using System;

namespace smartlocker.hardware.apiv2.Models
{
    public class Booking
    {
        public int BookingId { get; set; }
        public int LkRoomId { get; set; }
        public int OverTime { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string PassCode { get; set; }

    }
}