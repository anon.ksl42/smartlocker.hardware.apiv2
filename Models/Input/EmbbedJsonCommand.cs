﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLockerProject.Web.API.Models.Output
{
    public class EmbbedJsonCommand
    {
        public string RoomStatus { get; set; }
        public string Command { get; set; }
        public string RoomId { get; set; }
        public string Value { get; set; }

        public Object ResponseH(string Command, string RoomId , string RoomStatus) 
        {
            var result = new EmbbedJsonCommand
            {
                Command = Command,
                RoomId = RoomId,
                RoomStatus = RoomStatus
            };
            return result;
        }

    }


}
