namespace smartlocker.hardware.apiv2.Models
{
    public class Locker
    {
        public int? RegisterId { get; set; }
        public int LkRoomId { get; set; }
        public int LkSizeId { get; set; }
        public int LockerId { get; set; }
        public int ColumnPosition { get; set; }
        public int RowPosition { get; set; }
        public string LkRoomCode { get; set; }
        public string Status { get; set; }
        public string LkSizeName { get; set; }
    }
}