﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLockerProject.Web.API.Models.Output
{
    public class EmbbedJsonResponse
    {
        public string LockerRoom { get; set; }
        public string Status { get; set; }
        public string Detail { get; set; }
    }
}
