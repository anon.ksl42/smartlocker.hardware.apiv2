﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLockerProject.Web.API.Models.Output
{
    public class TokenResponse
    {
        public string Token { get; set; }
        public DateTime ExpiredDate { get; set; }
    }
}
