using System.Collections.Generic;
using Dapper;

namespace smartlocker.hardware.apiv2.Repositories.Interfaces
{
    public interface IBaseRepository
    {
        //Stored procedure
        IEnumerable<T> QueryStoreProcedure<T>(string spName, DynamicParameters dynamic);
        int ExecuteStoreProcedure<T>(string spName, DynamicParameters dynamic);

        //Query string
        IEnumerable<T> QueryString<T>(string sql);
        int ExecuteString<T>(string sql);
    }
}