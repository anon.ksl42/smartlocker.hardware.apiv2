using System;
using System.Collections.Generic;
using System.Linq;
using smartlocker.hardware.apiv2.Constants;
using smartlocker.hardware.apiv2.Models;
using smartlocker.hardware.apiv2.Repositories.Interfaces;
using smartlocker.hardware.apiv2.Services.Interfaces;
using smartlocker.hardware.apiv2.Services.Sql.Interfaces;

namespace smartlocker.hardware.apiv2.Services.Implements
{
    public class LockerService : ILockerService
    {
        private readonly IBaseRepository baseRepository;
        private readonly ISqlService sqlService;
         LkroomSize lkroomSize = new LkroomSize();
        public LockerService(IBaseRepository baseRepository, ISqlService sqlService)
        {
            this.baseRepository = baseRepository;
            this.sqlService = sqlService;
        }

        //refactored
        public void UpdateBooking(Booking booking )
        {
            if (booking.Status == "WP")
            {
                string date = DateTime.Now.ToString();
                var uQueryString = sqlService.updateBooking(booking.BookingId, "P", date);
                baseRepository.QueryString<Booking>(uQueryString).SingleOrDefault();
            }
            else if (booking.Status == "WF")
            {
                var uQueryString = sqlService.updateBooking(booking.BookingId, "F");
                baseRepository.QueryString<Booking>(uQueryString).SingleOrDefault();

                //update locker room back to standby status 
                var updateLkRoomScript = sqlService.UpdateLkRoom(booking.LkRoomId , "A");
                baseRepository.QueryString<Booking>(updateLkRoomScript).SingleOrDefault();
            }
            else
            {
                throw new Exception("Cannot update booking bacause status invalid");

            }
        }

        //refactored
        public Booking Authentication(string password, int lkRoomId)
        {
            string queryString = sqlService.selectBooking(password, lkRoomId);
            var result = baseRepository.QueryString<Booking>(queryString).FirstOrDefault();
            if(result != null)
            {
                return result;
            }
            else
            {
                throw new UnauthorizedAccessException();
            }
        }

        //refactored
        public bool checkBooking(int lkRoomId)
        {
            string queryString = sqlService.checkBookingStatus(lkRoomId);
            var result = baseRepository.QueryString<Booking>(queryString).SingleOrDefault();
            
            if (result != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Locker> GetAllLocker(int lockerId)
        {
            string queryString = sqlService.selectLocker(lockerId);
            List<Locker> result = baseRepository.QueryString<Locker>(queryString).ToList();
            if (result.Count == 0)
            {
                throw new ArgumentException("Locker id is incorrect");
            }
            List<Locker> resultAfterSort = new List<Locker>();
            foreach (string size in lkroomSize.Size)
            {
                List<Locker> lkRoomSize = result.Where(c => c.LkSizeName.Equals(size)).ToList();
                lkRoomSize.Sort((lk1, lk2) => lk1.LkRoomCode.CompareTo(lk2.LkRoomCode));
                lkRoomSize.ForEach((lkRoom) => resultAfterSort.Add(lkRoom));
            }
            for (int id = 0; id < resultAfterSort.Count;id++)
            {
                resultAfterSort[id].RegisterId = id ;
            }
           
            return result;
        }
    }
}