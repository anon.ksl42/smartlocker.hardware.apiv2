﻿using smartlocker.hardware.apiv2.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace smartlocker.hardware.apiv2.Services.Implements
{
    public class RegisterService : IRegisterService
    {
        enum RegisterIndex
        {
            GreenLight = 0,
            RedLight = 1,
            LockSwitch = 2,
            Buzzer = 3,
        }

        public int[] SetRegisterToWrite(int green, int red, int lockSwitch, int buzzer)
        {
            return new int[] { green, red, lockSwitch, buzzer };
        }

        public int GetRealStartAttr(int id)
        {
            return id * 4;
        }

        public int[] OpenLockerRoomProcess()
        {
            return new int[] { 0, 1, 1, 0 };
        }

        public int[] OpenLockerRoomFinish()
        {
            return new int[] { 1, 0, 1, 0 };
        }


    }
}
