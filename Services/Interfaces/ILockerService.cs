using System.Collections.Generic;
using smartlocker.hardware.apiv2.Models;

namespace smartlocker.hardware.apiv2.Services.Interfaces
{
    public interface ILockerService
    {
        Booking Authentication(string password, int lkRoomId);

        List<Locker> GetAllLocker(int lockerId);
        bool checkBooking(int lkRoomId);
        void UpdateBooking(Booking booking);
    }
}