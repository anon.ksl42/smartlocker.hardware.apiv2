﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace smartlocker.hardware.apiv2.Services.Interfaces
{
    public interface IRegisterService
    {
        int[] SetRegisterToWrite(int green, int red, int lockSwitch, int buzzer);
        int[] OpenLockerRoomProcess();
        int[] OpenLockerRoomFinish();
        int GetRealStartAttr(int id);
    }
}
