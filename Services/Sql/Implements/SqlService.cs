using System;
using smartlocker.hardware.apiv2.Services.Sql.Interfaces;

namespace smartlocker.hardware.apiv2.Services.Sql.Implements
{
    public class SqlService : ISqlService
    {

        public string updateLKRoom(int lkRoomId,string status)
        {
             string queryString = $@"UPDATE LOCKER_ROOMS
                                    SET Status = '{status}',UpdateDate = GETDATE() 
                                    WHERE LkRoomId = {lkRoomId}";
                                    Console.WriteLine(queryString);
            return queryString;
        }
        public string checkBookingStatus(int lkRoomId)
        {
            string queryString = $@"SELECT * FROM BOOKING
                                    WHERE LkRoomId = {lkRoomId} AND (Status = 'WP' OR Status = 'WF')";
                                    Console.WriteLine(queryString);
            return queryString;
        }

        public string selectBooking(string pwd, int lkRoomId)
        {
            string queryString = $@"SELECT * FROM BOOKING
                                    WHERE PassCode = '{pwd}' AND LkRoomId = {lkRoomId} AND (Status = 'WP' OR Status = 'WF')";
            return queryString;
        }

        //refactored
        public string selectLocker(int lockerId)
        {
            string queryString = $@"  SELECT L.LockerId
                                            ,LR.LkRoomId
                                            ,LR.LkRoomCode
                                            ,LR.[Status]
                                            ,LR.LkSizeId
                                            ,LS.LkSizeName
                                            ,LD.ColumnPosition
                                            ,LD.RowPosition
                                        FROM [SmartLocker].[dbo].[LOCKERS] L
                                        INNER JOIN LOCKER_ROOMS LR ON LR.LockerId = L.LockerId
                                        INNER JOIN LOCKER_DIAGRAMS LD ON LD.LkRoomId = LR.LkRoomId
                                        INNER JOIN LOCKER_SIZES LS ON LS.LkSizeId = LR.LkSizeId
                                        WHERE L.LockerId = {lockerId} AND L.Status = 'A'";
            return queryString;
        }

        public string updateBooking(int bookingId, string status, string date = null)
        {
            string updateDate = DateTime.Now.ToString();
            if (date != null)
            {
                string queryString = $@"UPDATE BOOKING
                                    SET Status = '{status}',UpdateDate = '{updateDate}',StartDate = '{date}' 
                                    WHERE BookingId = {bookingId}";

                return queryString;
            }
            else
            {
                string queryString = $@"UPDATE BOOKING
                                    SET Status = '{status}',UpdateDate = '{updateDate}' 
                                    WHERE BookingId = {bookingId}";

                return queryString;
            }
        }

        public string UpdateLkRoom(int lkRoomId , string status)
        {
           string queryString = $@"UPDATE LOCKER_ROOMS
                            SET Status = '{status}',UpdateDate = GETDATE()
                            WHERE LkRoomId = {lkRoomId}";
           return queryString;
        }
    }
}