using System;

namespace smartlocker.hardware.apiv2.Services.Sql.Interfaces
{
    public interface ISqlService
    {
        string selectBooking(string pwd, int lkRoomId);
        string updateBooking(int bookingId, string status, string date = null);
        string selectLocker(int lockerId);
        string checkBookingStatus(int lkRoomId);
        string UpdateLkRoom(int lkRoomId, string status);

    }
}