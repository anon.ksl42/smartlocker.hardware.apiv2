using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using smartlocker.hardware.apiv2.Repositories.Implements;
using smartlocker.hardware.apiv2.Repositories.Interfaces;
using smartlocker.hardware.apiv2.Services.Implements;
using smartlocker.hardware.apiv2.Services.Interfaces;
using smartlocker.hardware.apiv2.Services.Sql.Implements;
using smartlocker.hardware.apiv2.Services.Sql.Interfaces;

namespace smartlocker.hardware.apiv2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddScoped<IBaseRepository, BaseRepository>(c => new BaseRepository(Configuration.GetConnectionString("DefalutConnectionStrings")));
            services.AddScoped<ILockerService, LockerService>();
            services.AddScoped<IRegisterService, RegisterService>();
            services.AddScoped<ISqlService, SqlService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(x => x
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader());
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
